import RPi.GPIO as GPIO
import time
from gpiozero import LED
from ph_sensor import Sensor
from flow_sensor import FlowSensor
import Adafruit_DHT

# Configuración de los pines GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# Configuración de los pines para los sensores y actuadores
light_pin = 17
pump_pin = 27
valve_pin = 22
fan_pin = 23
ph_pin = 24
flow_pin = 25
temp_hum_pin = 4

# Configuración de los objetos de los sensores y actuadores
light = LED(light_pin)
pump = LED(pump_pin)
valve = LED(valve_pin)
fan = LED(fan_pin)
ph_sensor = Sensor(ph_pin)
flow_sensor = FlowSensor(flow_pin)
temp_hum_sensor = Adafruit_DHT.DHT22

# Configuración de los valores umbral para los sensores
ph_threshold = 6.0
flow_threshold = 2.0
temperature_threshold = 25
humidity_threshold = 60

# Funciones de control de los actuadores
def turn_on_light():
    light.on()

def turn_off_light():
    light.off()

def turn_on_pump():
    pump.on()

def turn_off_pump():
    pump.off()

def turn_on_valve():
    valve.on()

def turn_off_valve():
    valve.off()

def turn_on_fan():
    fan.on()

def turn_off_fan():
    fan.off()

# Funciones de lectura de los sensores
def read_ph():
    return ph_sensor.read()

def read_flow_rate():
    return flow_sensor.read()

def read_temperature_humidity():
    humidity, temperature = Adafruit_DHT.read_retry(temp_hum_sensor, temp_hum_pin)
    return humidity, temperature

# Función principal del programa
def main():
    while True:
        # Lectura de los sensores
        ph_value = read_ph()
        flow_rate = read_flow_rate()
        humidity, temperature = read_temperature_humidity()

        # Control de los actuadores
        if temperature > temperature_threshold:
            turn_on_fan()
        else:
            turn_off_fan()

        if ph_value < ph_threshold and flow_rate > flow_threshold:
            turn_on_pump()
            turn_on_valve()
        else:
            turn_off_pump()
            turn_off_valve()

        if humidity > humidity_threshold:
            turn_on_fan()
        else:
            turn_off_fan()

        if temperature < temperature_threshold and flow_rate < flow_threshold:
            turn_on_pump()
            turn_on_valve()
            time.sleep(10)
            turn_off_pump()
            turn_off_valve()
            time.sleep(10)

        if temperature < temperature_threshold and light.value == 0:
            turn_on_light()

        # Espera para la siguiente lectura
        time.sleep(10)

# Inicio del programa
if __name__ == "__main__":
    main()

# Limpieza de los pines GPIO al finalizar el programa
GPIO.cleanup()